Launching Spark 1.1.0 on Amazon EMR
===================================


The following are instructions for starting up an EMR cluster with Spark 1.1.0 using bootstrap actions.

-------------
Instructions
-------------

1. Utilize a bootstrap action to install the software  
     
	> Script: s3://support.elasticmapreduce/spark/install-spark

    > Arguments (optional): -v  &lt;spark_version&gt;

	> If no version is given, it will install the latest version available for the EMR Hadoop version.

	> Current versions available:
	
    > Hadoop Version  |        AMI        | Spark Version
	> --------------- | ----------------- | ----------------------
	> Hadoop 1.0.3    | AMI 2.x           | Spark 0.8.1
	> Hadoop 2.2.0    | AMI 3.0.x         | Spark 1.0.0
	> Hadoop 2.4.0    | AMI 3.1.x / 3.2.x | Spark 1.1.0b* 
	**(built with httpclient 4.2.5 to fix version conflict with AWS SDK)* 

	>Example of using with the older EMR ruby CLI:

    >`elastic-mapreduce --create --name spark --ami-version 3.2.1 --bootstrap-action s3://support.elasticmapreduce/spark/install-spark --instance-count 4 --instance-type m3.xlarge --alive will install Spark 1.1.0 on EMR AMI 3.2.1`
	

2. Utilize an EMR Step to start the Spark history server (optional)

	> 	Script: 	s3://support.elasticmapreduce/spark/start-history-server
	> (needs to be executed by
	> s3://elasticmapreduce/libs/script-runner/script-runner.jar)
	> 
	> 	Arguments: None
	> 
	> 	Currently works for Spark 1.x.
	> 
	> 	Example of using with the older EMR ruby CLI:
	> `elastic-mapreduce --create --name spark --ami-version 3.2.1 --bootstrap-action s3://support.elasticmapreduce/spark/install-spark --instance-count 4 --instance-type m3.xlarge --jar s3://elasticmapreduce/libs/script-runner/script-runner.jar --args "s3://support.elasticmapreduce/spark/start-history-server" --alive`


3. Utilize an EMR Step to configure the Spark default configuration (optional)

	

	> Script: 	s3://support.elasticmapreduce/spark/configure-spark.bash
	> (needs to be executed by s3://elasticmapreduce/libs/script-runner/script-runner.jar) 
		
	> 	Arguments: A key=value pair of configuration items to add or replace in spark-defaults.conf file
	
	> Example of using with the older EMR ruby CLI:
	> `elastic-mapreduce --create --name spark --ami-version 3.2.1 --bootstrap-action s3://support.elasticmapreduce/spark/install-spark --instance-count 4 --instance-type m3.xlarge --jar s3://elasticmapreduce/libs/script-runner/script-runner.jar --args "s3://support.elasticmapreduce/spark/start-history-server" --jar s3://elasticmapreduce/libs/script-runner/script-runner.jar --args "s3://support.elasticmapreduce/spark/configure-spark.bash,spark.default.parallelism=4800,spark.locality.wait.rack=0" --alive`


----------
> 
> **Note:**

>  If you intend to use Hive statements in Spark SQL then you will need to run the following commands (or use the modified version of the install-spark script that is in this repo):

> `cp /home/hadoop/hive/conf/hive-default.xml /home/hadoop/spark/conf/hive-site.xml`
>  
>  `sed -i 's/SPARK_CLASSPATH=\"/&\/home\/hadoop\/hive\/lib\/bonecp-0.8.0.RELEASE.jar:\/home\/hadoop\/hive\/lib\/mysql-connector-java-5.1.30.jar:/' /home/hadoop/spark/conf/spark-env.sh`
>  

----------


